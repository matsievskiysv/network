# Установка системы

## OrangePI R1

<img src="../images/r1-1.jpg" width="400">

Коробка с сервером

<img src="../images/r1-2.jpg" width="400">

Подключение USB-USART переходника к компьютеру

<img src="../images/r1-4.jpg" width="400">

Подключение USB-USART переходника к серверу

<img src="../images/r1-3.jpg" width="400">

Подключённые к серверу Ethernet кабели

<img src="../images/r1.png" width="400">

Расположение интерфейсов на сервере

---

Требуется определить путь к SD карте, на которую будет записана система.
```bash
lsblk
```

Ищем в списке карту, запоминаем её название.
Для имени карты `sdc` путь будет `/dev/sdc`.

Скачиваем образ Debian с <https://www.armbian.com/orange-pi-r1/> и загружаем его на SD карту
```bash
sudo dd if=Armbian_Orangepi-r1.img of=/dev/sdc bs=1M status=progress
sync
```

Первоначально общение с OrangePI R1 может происходить только при помощи USB-USART переходника.
Подключаем переходник к серверу согласно таблице

| server | converter |
|:-:|:-:|
| RX | TX |
| TX | RX |
| GND | GND |

После подключения переходника вставляйте SD карту в слот, подключайте питание сервера, вставляйте
переходник в компьютер, а Ethernet кабель в любой разъём сервера.

Для подключения к переходнику
```bash
screen /dev/ttyUSB0 115200
```

На экране должна появиться консоль сервера.

После первой загрузки системы появится надпись `reboot as soon as possible`.
Перезагружаемся
```bash
systemctl reboot
```

Задаём информацию о новом пользователе и пароле.

Открываем файл `/boot/armbianEnv.txt`
```bash
sudo nano /boot/armbianEnv.txt
```

И добавляем туда строку
```ini
extraargs=net.ifnames=1
```

И перезагружаем сервер
```bash
systemctl reboot
```

Теперь требуется узнать имена Ethernet портов.
```bash
ip a
```

Для определённости, пусть они называются `ens4` и `ens5`.

Удаляем ненужный `network-manager`
```bash
sudo apt remove network-manager
```

А в файл `/etc/network/interfaces` добавляем
```
auto ens4
allow-hotplug ens4
iface ens4 inet dhcp

auto ens5
allow-hotplug ens4
iface ens4 inet dhcp
```

И перезапускаем сервис
```bash
systemctl restart networking.service
```

После перезагрузки сервер должен подключиться к интернету с использованием `DHCP`.

## Gigabyte

### Подготовка установочной флешки

Скачиваем образ установщика с <https://www.debian.org/>.

Требуется определить путь к установочной флешке.
```bash
lsblk
```
Ищем в списке флешку, запоминаем её название.
Для имени флешки `sdc` путь будет `/dev/sdc`.

Заливаем образ установочного диска на флешку:
```bash
sudo dd if=debian-netinst.iso of=/dev/sdc bs=1M status=progress
sync
```

При установке система может потребовать дополнительный драйвер.
В этом случае архив с драйвером ищется по названию [тут](https://www.debian.org/distrib/packages), и
копируется в корень другой флешки.

Установочная флешка готова.
Втыкаем её в сервер, загружаемся и следуем инструкциям установщика.

В `BIOS` устройства выбираем опцию автовключение после подачи питания, если такая есть.

### Настройка sudo

Если зашли через обычного пользователя, переходим в root:
```bash
su
```
и вводим пароль.

Для определённости, имя пользователя будет `admin`.
Устанавливаем `sudo` и добавляем пользователя в группу `sudo`:
```bash
apt update && apt install sudo
/usr/sbin/adduser admin sudo
```

Выходим из `root` и перезаходим в пользователя для применения настроек.

Устанавливаем `polkit`
```bash
sudo apt install libpolkit-agent-1-0
```

# Обновление системы

Добавляем пользователя в группу `adm`
```bash
sudo adduser admin adm
```

И перезаходим в пользователя.

После установки требуется обновить систему.

В файл `/etc/apt/sources.list` пишем:
```
deb http://mirror.mephi.ru/debian/ stable main
deb-src http://mirror.mephi.ru/debian/ stable main

deb http://mirror.mephi.ru/debian/ stable-backports main
deb-src http://mirror.mephi.ru/debian/ stable-backports main

deb http://deb.debian.org/debian-security/ stable-security main
deb-src http://deb.debian.org/debian-security/ stable-security main

deb http://mirror.mephi.ru/debian/ stable-updates main
deb-src http://mirror.mephi.ru/debian/ stable-updates main
```

Для OrangePi R1 требуется также добавить строку
```
deb http://apt.armbian.com bullseye main bullseye-utils bullseye-desktop
```

Потом выполняем команды
```bash
sudo apt update && sudo apt upgrade -y && sudo apt autoremove
```

И перезагружаем сервер после установки.

# Настройка shell

```bash
sudo apt install zsh curl git
```

Записываем в `$HOME/.zshrc`:
```bash
if [[ ! -f "$HOME/.antigen/antigen.zsh" ]]; then
    mkdir -p "$HOME/.antigen" \
        && curl -L git.io/antigen > "$HOME/.antigen/antigen.zsh"
fi

source ~/.antigen/antigen.zsh
antigen use oh-my-zsh
antigen theme fino

antigen bundle git
antigen bundle command-not-found
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle zsh-users/zsh-syntax-highlighting

antigen bundle "$HOME/.zsh_completion.d/"

antigen apply

if [ -r "$HOME/.profile" ];then
  . "$HOME/.profile"
fi
```

Выберем `zsh`

```bash
chsh -s $(which zsh)
```

Установим терминальный мультиплексер
```bash
sudo apt install dvtm
```

Теперь можно зайти в `dvtm`
```bash
dvtm
```

и использовать несколько окон терминала одновременно.
Подробнее о `dvtm` описано в
```bash
man dvtm
```

# Настройки системы

Выполнить команды

```bash
sudo sed -i 's/.*PermitRootLogin.*/PermitRootLogin no/g' /etc/ssh/sshd_config
```

# Network

Для начала определим названия интерфейсов WAN (смотрит наружу) и LAN (смотрит внутрь).

```bash
ip a
```

Для определённости интерфейс WAN будет называться `ens4`, интерфейс LAN -- `ens5`.

В файле `/etc/network/interfaces` записываем:
```
source /etc/network/interfaces.d/*
# Network is managed by Network manager
auto lo
iface lo inet loopback

# WAN
auto ens4
allow-hotplug ens4
iface ens4 inet static
	address 85.143.113.112/24
	gateway 85.143.113.1
	hwaddress ether 00:a0:c9:df:82:cc

# LAN
auto ens5
allow-hotplug ens5
iface ens5 inet static
	address 192.168.100.1/24
```

Здесь `85.143.113.112/24` -- реальный IP адрес сервера, `00:a0:c9:df:82:cc` -- его (подложный) MAC
адрес, `192.168.100.1` -- внутренней IP адрес.

Добавляем адреса DNS серверов в `/etc/resolv.conf`
```
nameserver 77.88.8.88
nameserver 77.88.8.2
nameserver 8.8.8.8
```

Сервера можно выбрать любые, но не больше 3х.

# DHCP сервер

Устанавливаем DHCP сервер
```bash
sudo apt install isc-dhcp-server
```

В файле `/etc/default/isc-dhcp-server` в поле `INTERFACESv4` прописываем название LAN интерфейса
```ini
INTERFACESv4="ens5"
```

В файле `/etc/dhcp/dhcpd.conf` записываем
```
option domain-name "networkname.mephi.ru";
option domain-name-servers 77.88.8.88, 77.88.8.2, 8.8.8.8;
option subnet-mask 255.255.255.0;

default-lease-time 600;
max-lease-time 7200;

ddns-update-style none;
authoritative;

subnet 192.168.100.0 netmask 255.255.255.0 {
  range 192.168.100.30 192.168.100.200;
  option routers 192.168.100.1;
}
```

`domain-name` можно выбрать любым. `domain-name-servers` лучше взять те-же, что и в
`/etc/resolv.conf`.

Начало области DHCP адресов (в этом примере `192.168.100.30`) надо поставить таким, чтобы хватило
места для статически заданных IP (об этом подробнее в мануале по администрированию).

Включаем DHCP сервер
```bash
systemctl start isc-dhcp-server.service
systemctl enable isc-dhcp-server.service
```

Теперь сервер можно выключить и поставить на своё рабочее место.
Подключиться к нему можно из локальной сети по ssh:
```bash
ssh admin@192.168.100.1
```

# Netfilter

Разрешим перенаправление пакетов
```bash
sudo sysctl net/ipv4/ip_forward=1
```

И сохраним настройку, создав файл `/etc/sysctl.d/myconf.conf` с содержимым
```ini
net.ipv4.ip_forward=1
```

Устанавливаем `nft`
```bash
sudo apt install nftables
```

В файле `/etc/nftables.conf` записываем
```
#!/usr/sbin/nft -f

flush ruleset

define lan = ens5
define wan = ens4
define vpn = tun0
define lan_addresses = 192.168.100.0/24
define vpn_addresses = 10.8.0.0/24

table firewall {
	map hosts {
                type ipv4_addr . ether_addr : verdict
                elements = {
			192.168.100.2  . 50:e5:49:30:36:65 : accept ,
		}
        }
	set remote_allowed {
                type ipv4_addr
                elements = { 85.143.113.112 , 85.143.113.113 }
        }
	chain prerouting {
		type nat hook prerouting priority 0; policy accept;
	}
	chain postrouting {
		type nat hook postrouting priority 100; policy accept;
		ip saddr $lan_addresses oifname $wan masquerade
		ip saddr $vpn_addresses ip daddr != $vpn_addresses oifname != $vpn masquerade
	}
	chain input {
		type filter hook input priority 0; policy drop;
		# drop invalid, allow established
		ct state vmap {invalid : drop, established : accept, related : accept}
		# allow loopback
		iifname "lo" accept
		# allow ping from LAN
		iifname $lan ip saddr $lan_addresses ip protocol icmp icmp type echo-request accept
		# allow ping from VPN
		iifname $vpn ip saddr $vpn_addresses ip protocol icmp icmp type echo-request accept
		# open Radicale port
		ip protocol tcp tcp dport 5232 accept
		# open VPN port
		iifname != $vpn ip protocol tcp tcp dport 1194 accept
		# allow SSH from LAN and VPN
		iifname != $wan ip protocol tcp tcp dport 22 accept
		# allow connecting to CUPS
		iifname $lan ip protocol tcp tcp dport 631 accept
		# allow connect to samba
		iifname $lan ip protocol tcp tcp dport 139 accept
		iifname $lan ip protocol tcp tcp dport 445 accept
		# allow SSH from allowed remotes
		iifname $wan ip protocol tcp ip saddr @remote_allowed tcp dport 22 accept
		# open SQUID, DHCP port for lan
		iifname $lan ip protocol tcp ip saddr $lan_addresses tcp dport {3128, 67} accept
		# open SQUID, DHCP port for vpn
		iifname $vpn ip protocol tcp ip saddr $vpn_addresses tcp dport {3128, 67} accept
		# LAN nice reject
		iifname != $wan ip saddr $lan_addresses reject with icmp type host-prohibited
	}
	chain forward {
		type filter hook forward priority 0; policy drop;
		ct state {established,related} accept
		iifname $vpn oifname $lan accept
		iifname $lan oifname $wan ip saddr . ether saddr vmap @hosts
		iifname {$vpn, $lan} oifname $wan reject
	}
	chain output {
		type filter hook output priority 0; policy accept;
	}
}
```

В этом файле надо выставить правильные названия WAN и LAN интерфейсов.
Другие настройки подробнее освещены в инструкции по администрированию.

Перезагружаем фильтр
```bash
systemctl restart nftables.service
```

Теперь сервер должен перестать быть видимым со стороны WAN, внутренние устройства должны видеть друг
друга, но не мочь выйти в интернет.

# Proxy сервер

Устанавливаем proxy сервер
```bash
sudo apt install squid apache2-utils
```

Добавляем в `/etc/squid/squid.conf`
```
include /etc/squid/conf.d/*.conf

auth_param basic program /usr/lib/squid/basic_ncsa_auth /etc/squid/passwords
auth_param basic children 10 startup=0 idle=1
auth_param basic realm Squid proxy-caching web server
auth_param basic credentialsttl 4 hours

acl password proxy_auth REQUIRED

acl localnet src 192.168.100.0/24
acl localnet src 10.8.0.0/24	# VPN
acl SSL_ports port 443          # https
acl Safe_ports port 80		# http
acl Safe_ports port 443		# https
acl CONNECT method CONNECT

http_access deny !Safe_ports

http_access allow localhost manager
http_access deny manager

http_access allow password localnet
http_access deny all

http_port 3128

coredump_dir /var/spool/squid

refresh_pattern ^ftp:		1440	20%	10080
refresh_pattern ^gopher:	1440	0%	1440
refresh_pattern -i (/cgi-bin/|\?) 0	0%	0
refresh_pattern .		0	20%	4320
```

Создаём файл с паролями
```bash
sudo touch /etc/squid/passwords
```

И перезапускаем сервер
```bash
systemctl status squid.service
```

При работе с OrangePI R1 логи хранятся в специальной области RAM, ёмкость которой мала.
При заполнении этой области могут возникать проблемы с работой в том числе и squid.
Для более стабильной работы установим ограничение на размер логов `journalctl`

В файле `etc/systemd/journald.conf` откомментируем строку
```
SystemMaxUse=10M
```

# VPN сервер

```bash
sudo apt install easy-rsa openvpn
```

Создадим рабочую папку

```bash
mkdir -p ~/openvpn/files
```

В файле `~/openvpn/vars` откомментируем строки
```
set_var EASYRSA_REQ_COUNTRY	"RU"
set_var EASYRSA_REQ_PROVINCE	"Moscow"
set_var EASYRSA_REQ_CITY	"Moscow"
set_var EASYRSA_REQ_ORG	"MEPhI"
set_var EASYRSA_REQ_EMAIL	"mail@example.com"
set_var EASYRSA_REQ_OU		"KAF14"
set_var EASYRSA_CRL_DAYS	365
```

Сгенерируем сертификаты
```bash
cd ~/openvpn
/usr/share/easy-rsa/easyrsa init-pki
/usr/share/easy-rsa/easyrsa build-ca nopass
/usr/share/easy-rsa/easyrsa gen-req server nopass
/usr/share/easy-rsa/easyrsa sign-req server server
sudo cp -t /etc/openvpn ~/openvpn/pki/ca.crt ~/openvpn/pki/issued/server.crt ~/openvpn/pki/private/{server.key,ca.key}
/usr/share/easy-rsa/easyrsa gen-dh
sudo cp -t /etc/openvpn ~/openvpn/pki/dh.pem
sudo openvpn --genkey secret pki/ta.key
sudo cp -t /etc/openvpn ~/openvpn/pki/ta.key
sudo chgrp adm pki/ta.key
sudo chmod 660 pki/ta.key
/usr/share/easy-rsa/easyrsa gen-crl
sudo cp -t /etc/openvpn ~/openvpn/pki/crl.pem
```

Скопируем настройки сервера

```bash
sudo cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf /etc/openvpn
```

И в файле `/etc/openvpn/server.conf` найдём и заменим строки с настройками
```bash
proto tcp
;proto udp

topology subnet

push "route 192.168.100.0 255.255.255.0"

tls-auth ta.key 0

cipher AES-256-CBC
auth SHA256

dh dh.pem

;explicit-exit-notify 1

crl-verify crl.pem
```

Включим сервис
```bash
systemctl start openvpn@server
systemctl enable openvpn@server
```

Скопируем настройки клиента

```bash
cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf ~/openvpn/base.conf
```

И в файле `~/openvpn/base.conf` найдём и заменим строки с настройками
```bash
proto tcp
;proto udp

remote 85.143.113.114 1194

user nobody
group nogroup

;ca ca.crt
;cert client.crt
;key client.key

;tls-auth ta.key 1

key-direction 1

; script-security 2
; up /etc/openvpn/update-resolv-conf
; down /etc/openvpn/update-resolv-conf
```

Наконец, запишем файл `~/openvpn/make_cert.sh`
```bash
#!/bin/bash

# First argument: Client identifier

EASYRSA=./easyrsa

PKI_DIR=pki
PRIVATE_DIR=pki/private
ISSUED_DIR=pki/issued
OUTPUT_DIR=files
BASE_CONFIG=base.conf

COUNTRY=RU
STATE=Moscow
CITY=Moscow
ORGAMIZATION=MEPhI
EMAIL=email@example.com
UNIT=MYUNIT
DAYS=${DAYS:-180}

${EASYRSA} --batch --req-c=${COUNTRY} --req-st=${STATE} --req-city=${CITY} --req-org=${ORGAMIZATION} --req-email=${EMAIL} --req-ou=${UNIT} --days=${DAYS} --req-cn=${1} gen-req ${1} nopass
${EASYRSA} --batch --req-c=${COUNTRY} --req-st=${STATE} --req-city=${CITY} --req-org=${ORGAMIZATION} --req-email=${EMAIL} --req-ou=${UNIT} --days=${DAYS} --req-cn=${1} sign-req client ${1}

cat ${BASE_CONFIG} \
    <(echo -e "\n# Contact email: ${EMAIL}") \
    <(echo -e '<ca>') \
    ${PKI_DIR}/ca.crt \
    <(echo -e '</ca>\n<cert>') \
    ${ISSUED_DIR}/${1}.crt \
    <(echo -e '</cert>\n<key>') \
    ${PRIVATE_DIR}/${1}.key \
    <(echo -e '</key>\n<tls-auth>') \
    ta.key \
    <(echo -e '</tls-auth>') \
    > ${OUTPUT_DIR}/${1}.ovpn
```

```bash
chmod 700 ~/openvpn
chmod 700 ~/openvpn/make_cert.sh
```

## Оповещения об устаревших сертификатах

Устанавливаем требуемые программы

```bash
sudo apt install python3 cron
```

Для оповещения об устаревших сертификатах по почте требуется скопировать программу
```bash
git clone https://gitlab.com/matsievskiysv/certcheck.git
```

В файле настроек `~/certcheck/certcheck.ini` в разделе `[email]` требуется указать настройки SMTP
сервера, с которого будут отправляться письма.
В разделе `[certificates]` надо указать путь к сертификату `crl` и пользовательским сертификатам.
Для пользователя `admin` они будут находиться в `/home/admin/openvpn/pki/crl.pem` и
`/home/admin/openvpn/pki/issued/*.crt` соответственно.

Теперь надо настроить вызов программы при помощи `cron`.
Для этого используем команду
```bash
crontab -e
```
и добавляем строку
```
0 7 * * * /home/admin/certcheck/certcheck.py /home/admin/certcheck/certcheck.ini
```
После добавления этой строки программа будет проверять сертификаты каждый день в `7:00`.

## Оповещения о проблемах с системой

Устанавливаем требуемые программы

```bash
sudo apt install cron jq smartmontools
```

Для оповещения о проблемах с системой по почте требуется скопировать программу
```bash
git clone https://gitlab.com/matsievskiysv/server-notifier.git
```

В файле настроек `~/server-notifier/notifier.ini` в разделе `[email]` требуется указать настройки SMTP
сервера, с которого будут отправляться письма.

Теперь надо настроить вызов программы при помощи `cron`.
Для этого используем команду
```bash
crontab -e
```
и добавляем строки
```
0 7 * * * /home/admin/server-notifier/notifier.py /home/admin/server-notifier/notifier.ini /home/admin/server-notifier/scripts/disk_usage.sh
0 7 * * * /home/admin/server-notifier/notifier.py /home/admin/server-notifier/notifier.ini /home/admin/server-notifier/scripts/systemd_degraded.sh
0 7 * * * /home/admin/server-notifier/notifier.py /home/admin/server-notifier/notifier.ini /home/admin/server-notifier/scripts/check_mail.sh
```

И добавляем часть команд, запускаемых от имени `root`
```bash
sudo crontab -e
```
и добавляем строки
```
0 7 * * * /home/admin/server-notifier/notifier.py /home/admin/server-notifier/notifier.ini /home/admin/server-notifier/scripts/check_mail.sh
0 7 * * 1 /home/admin/server-notifier/notifier.py /home/admin/server-notifier/notifier.ini /home/admin/server-notifier/scripts/upgradable.sh
0 7 * * * /home/admin/server-notifier/notifier.py /home/admin/server-notifier/notifier.ini /home/admin/server-notifier/scripts/smartmon.sh
0 1 * * * /home/admin/server-notifier/scripts/smartmon_short.sh
0 23 * * 6 /home/admin/server-notifier/scripts/smartmon_long.sh
```

После добавления этих строк программа будет проверять занятое место на дисках, обновления системы, состояние сервисов и системную почту в соответствии с расписанием.

Короткая проверка дисков будет производиться каждый день в час ночи, длинная -- в субботу в 11 вечера.

## Оповещение о перезагрузке

Выполнить команду

```bash
sudo systemctl edit --force --full reboot-notifier
```

Добавить строки

```
[Unit]
Description=Reboot notifier
After=network.target

[Service]
Type=oneshot
ExecStart=/home/admin/server-notifier/notifier.py /home/admin/server-notifier/notifier.ini /home/admin/server-notifier/scripts/reboot.sh

[Install]
WantedBy=multi-user.target
```

Выполнить команду

```bash
sudo systemctl enable reboot-notifier
```

## Оповещение о входе в систему

Добавить файл `/etc/pam.d/login-notify`

```
session optional pam_exec.so seteuid /home/admin/server-notifier/notifier.py /home/admin/server-notifier/notifier.ini /home/admin/server-notifier/scripts/login.sh
```

В конце файлов `/etc/pam.d/login` и `/etc/pam.d/sshd` добавить строку

```
@include login-notify
```

# CALDAV сервер

## Получение сертификата

Установим `socat`
```bash
sudo apt install socat
```
и `acme.sh`, указав рабочий email
```bash
curl https://get.acme.sh | sh -s email=rflab@mephi.ru
```

# Подключение к серверу openkm

# Настройка openkm

# Общие папки

# Сканер

# Принтер

Для использования подключённого к серверу по USB принтера требуется установить программы
```bash
sudo apt install printer-driver-all cups samba
```
В файле `/etc/samba/smb.conf` находим строки, относящиеся к принтерам
```
[printers]
   comment = All Printers
   browseable = yes
   path = /var/spool/samba
   printable = yes
   guest ok = yes
   read only = yes
   create mask = 0700

[print$]
   comment = Printer Drivers
   path = /var/lib/samba/printers
   browseable = yes
   read only = yes
   guest ok = yes
```

Разрешаем удалённый доступ к CUPS
```bash
sudo cupsctl --remote-admin --remote-any --share-printers
```

Перезагружаем сервисы
```bash
systemctl restart smbd nmbd cups
```

Теперь настройку принтеров можно производить с компьютеров локальной сети по адресу
<http://192.168.100.1:631/> под пользователем `root` и соответствующим паролем.
